// Action click button "Connect"
// send params to server

document.addEventListener("DOMContentLoaded", _init);


function _init() {
    console.log('_init');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    getInitStatus();
    // get pereodic data
    setInterval(getStatus, 3000);
}

function getStatus() {
    console.log('getStatus()')
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    let fetchRes = fetch(Settings.url + 'api/status/get', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        Settings.params = d;
        //console.log(d, Settings)
        //Settings.params.console = ['row!', new Date().getTime()]
        applyParams();
        workStatus();
        writeConsole();

    })
}

function getInitStatus() {
    console.log('getInitStatus()')
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    let fetchRes = fetch(Settings.url + 'api/status/get', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        //Settings.params = d;
        Settings.params = d;
        console.log(d, Settings)
        applyParams();
        loadSettings();
        workStatus();
    })
}


function sendConnect() {
    let res = getFormData();
    let error = '';
    console.log('function sendConnect()',res)

    // let elInvalid = document.querySelector('.is-invalid');
    // console.log(elInvalid);


    let selectBox = document.querySelector('#elPorts');
    selectBox.classList.remove('is-invalid');
    let inputTcpPort = document.querySelector('#elNtripTcpPort');
    inputTcpPort.classList.remove('is-invalid');

    if(res.port_name == '') {
        error = true;
        selectBox.classList.add('is-invalid');
    }
    if(res.ntrip.tcp == 'on' && !res.ntrip.tcp_port) {
        error = true;
        inputTcpPort.classList.add('is-invalid');
    }

    if(error) {
        showToast('Не заполнены необходимые данные', 'error');
        return;
    }


    // send to server
    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };
    let fetchRes = fetch(Settings.url + 'api/pisatel/start', options);
    fetchRes.then(res =>
        res.json()).then(d => {
            if(d.status == 'ok') {
                Settings.params.work_status = 'processing';
                workStatus();
            }
        console.log('sendConnect()->result', d);
        showToast(d.message, d.status);
    })
}

function sendStop() {
    let res = getFormData();
    console.log('function sendStop()',res)
    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };
    let fetchRes = fetch(Settings.url + 'api/pisatel/stop', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            Settings.params.work_status = 'default';
            workStatus();
        }
        console.log('sendStop()->result', d);
        showToast(d.message, d.status);
    })
}


function workStatus() {
    console.log('workStatus()', Settings);
    if(Settings.params.work_status == 'default') {
        document.getElementById('btnConnect').classList.remove('visually-hidden');
        document.getElementById('btnProcess').classList.add('visually-hidden');
        document.getElementById('btnError').classList.add('visually-hidden');
    } else if(Settings.params.work_status == 'processing') {
        document.getElementById('btnConnect').classList.add('visually-hidden');
        document.getElementById('btnProcess').classList.remove('visually-hidden');
        document.getElementById('btnError').classList.add('visually-hidden');
    } else if(Settings.params.work_status == 'error') {
        document.getElementById('btnConnect').classList.add('visually-hidden');
        document.getElementById('btnProcess').classList.add('visually-hidden');
        document.getElementById('btnError').classList.remove('visually-hidden');
    }
}

function loadSettings() {
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    let fetchRes = fetch(Settings.url + 'api/params/get', options);

    fetchRes.then(res =>
        res.json()).then(d => {
            Settings.params.coords_status = d.coords_status;
            Settings.params.coords_input  = d.coords_input;
            console.log(d, Settings)
            applyForms();
    })
}


function saveSettings() {
    let res = getFormData();

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };
    let fetchRes = fetch(Settings.url + 'api/params/set', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        console.log('saveSettings()->result', d);
        showToast(d.message, d.status);
    })
}



function sendAuto() {

}


function applyPorts() {
    console.log('applyPorts()')
}

function applyForms() {
    console.log('applyForms()')
    document.getElementById('elCoordsLat').value    = Settings.params.coords_input[0];
    document.getElementById('elCoordsLon').value    = Settings.params.coords_input[1];
    document.getElementById('elCoordsHeight').value = Settings.params.coords_input[2];
    document.getElementById('elCoordsStatus').checked =
        Settings.params.coords_status == 'fix' ? false : true;


    let selectBox = document.querySelector('#elPorts');
    while (selectBox.options.length > 1) {
        selectBox.remove(1);
    }

    Settings.params.ports.forEach((element) => {
        let newOption = new Option(element,element);
        selectBox.add(newOption, undefined);
    });



}

function applyParams() {
    console.log('applyParams()')
    document.getElementById('elSerial').value =
        Settings.params.serial_number != 'Undefs'
        ? Settings.params.serial_number : '';
    document.getElementById('elSats').value   =
        Settings.params.sats_data != 'Undef'
        ? Settings.params.sats_data : '';
    document.getElementById('elFix').value    =
        Settings.params.fix_status != 'Undef'
        ? Settings.params.fix_status : '';

    document.getElementById('paramNtripStatus').textContent = Settings.params.ntrip_status;
    document.getElementById('paramFileStatus').textContent = Settings.params.file_status;
    document.getElementById('paramLat').textContent = Settings.params.coords[0];
    document.getElementById('paramLon').textContent = Settings.params.coords[1];
    document.getElementById('paramHeight').textContent = Settings.params.coords[2];
}


function writeConsole() {
    let elConsole = document.getElementById('elConsole');
    let textConsole = Settings.params.console.join("\n");
    if(textConsole) {
        elConsole.textContent = elConsole.textContent + textConsole + "\n";
        elConsole.scrollTop = elConsole.scrollHeight;
        Settings.params.console = []
    }
}





function getFormData() {
    let res = {};
    res.port_name = document.getElementById('elPorts').selectedOptions[0].value;
    res.ntrip = {};
    res.ntrip.status = document.querySelector('input[name="elNtripStatus"]:checked').value;
    res.ntrip.tcp = document.getElementById('elNtripTcp').checked
        ? document.getElementById('elNtripTcp').value
        : 'off';
    res.ntrip.tcp_port = document.getElementById('elNtripTcpPort').value;
    res.coords_input = [];
    res.coords_status = document.getElementById('elCoordsStatus').checked ? 'auto' : 'fix';
    res.coords_input.push(parseFloat(document.getElementById('elCoordsLat').value));
    res.coords_input.push(parseFloat(document.getElementById('elCoordsLon').value));
    res.coords_input.push(parseFloat(document.getElementById('elCoordsHeight').value));

    return res;
}
