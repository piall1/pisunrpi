#!/bin/bash
SYSTEMD_PATH="/etc/systemd/system/"
SCRIPT_PATH="$(realpath "$0")"
SCRIPT_DIR="$(dirname "$SCRIPT_PATH")"
UNIT_SYSTEMD="$SCRIPT_DIR/pisun_rpi.service"

echo $UNIT_SYSTEMD
echo $SYSTEMD_PATH
echo $SCRIPT_PATH
echo $SCRIPT_DIR

cd "$SCRIPT_DIR"
cd "./backend"
source venv/bin/activate
sudo apt -y update
sudo apt -y install git
sudo apt -y install python3 python3-pip
sudo pip3 install psutil pyserial
deactivate
cd "$SCRIPT_DIR"
sed -i "s|launch_path|$SCRIPT_DIR|g" "$UNIT_SYSTEMD"
sudo cp $UNIT_SYSTEMD $SYSTEMD_PATH
sudo systemctl daemon-reload
sudo systemctl enable pisun_rpi.service
sudo systemctl start pisun_rpi.service