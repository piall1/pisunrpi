import json
import os


class WebJson:
   
    def create_status(self, work_status, coords, ntrip_status, file_status, ports, serial_number, sats_data, 
                      fix_status, console):
        status = {
            "work_status": work_status,
            "coords": coords,
            "ntrip_status": ntrip_status, 
            "file_status": file_status,
            "ports": ports,
            "serial_number": serial_number,
            "sats_data": sats_data,
            "fix_status": fix_status,
            "console": console,
        }
        return json.dumps(status, ensure_ascii=False, indent=4)
    
    def create_params(self, coords, coords_status):
        params = {
            "coords_status": coords_status,
            "coords_input": [coords[0], coords[1], coords[2]]
        }
        return json.dumps(params, ensure_ascii=False, indent=4)

    def create_filelist(self, path):
        file_list = []

        try:
            for filename in os.listdir(path):
                file_path = os.path.join(path, filename)
                if os.path.isfile(file_path):
                    size = os.path.getsize(file_path)
                    time_mark = int(os.path.getmtime(file_path))

                    file_info = {
                        "filename": filename,
                        "size": size,
                        "time_mark": time_mark
                    }
                    file_list.append(file_info)
        
        except Exception as e:
            print(f"Ошибка при создании списка файлов: {e}")
        
        sorted_file_list = sorted(file_list, key=lambda x: x["time_mark"], reverse=True)
        json_data = {"files": sorted_file_list}
        return json.dumps(json_data, indent=4)

    def parce_params(self, string):
        decoded_data = json.loads(string)
        
        coords_status = decoded_data['coords_status']
        coords = decoded_data['coords_input']
        return coords_status, coords
    
    def parce_start_message(self, string):
        decoded_data = json.loads(string)
        
        ntrip = decoded_data["ntrip"]
        
        port_name = decoded_data["port_name"]
        ntrip_status = ntrip["status"]
        tcp = ntrip["tcp"]
        tcp_port = ntrip["tcp_port"]
        
        coords_status = decoded_data["coords_status"]
        coords_input = decoded_data["coords_input"]
        return port_name, ntrip_status, tcp, tcp_port, coords_status, coords_input
    
    def parce_delete_file(self, string):
        decoded_data = json.loads(string)
        
        file = decoded_data["filename"]
        
        return str(file)
        