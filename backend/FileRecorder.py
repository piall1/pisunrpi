import datetime
import os

class FileRecorder:

    def __init__(self):
        self.file_path = "../files/"
        self.filename = ""
        self.status_file = "close"
        self.format_file = "rtcm3"
        if not os.path.exists(self.file_path):
            os.makedirs(self.file_path)
        else:
            pass

    def make_name(self):
        current_datetime = datetime.datetime.now()
        year = current_datetime.year
        month = current_datetime.month
        day = current_datetime.day
        hour = current_datetime.hour
        minute = current_datetime.minute
        second = current_datetime.second

        name_file = f"{year:04}{month:02}{day:02}{hour:02}{minute:02}{second:02}.{self.format_file}"
        self.filename = name_file
        print("filename: ", name_file)

    def open_file(self):
        if self.status_file == "close":
            try:
                self.file = open(self.file_path + self.filename, "wb")
                self.status_file = "open"
            except Exception as e:
                print(f"Error opening file: {e}")   

    def close_file(self):
        if self.status_file == "open":
            try:
                self.file.close()
                self.status_file = "close"
                self.filename = ""
            except Exception as e:
                print(f"Error closing file: {e}") 
        
    def write(self,binary_data):
        if self.status_file == "open":
            try:
                self.file.write(binary_data)
                self.file.flush()
            except Exception as e:
                print(f"Error writing to file: {e}")
