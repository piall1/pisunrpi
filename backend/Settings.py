import math


class SettingsManager:

    def __init__(self):
        self.coords_status = "auto"
        self.coords = [None, None, None]
        self.settings = [
            b"unlogall\r\n",
            b"unlogall\r\n",
            b"rtkrefmode 0\r\n",
            b"ECUTOFF 0.0\r\n",
            b"BD2ECUTOFF 0\r\n",
            b"UNDULATION USER 0.000\r\n"
            b"log version ontime 1\r\n",
            b"log bestposa ontime 1\r\n",
            b"Log gptra ontime 1\r\n",
            b"log rtcm1006b ontime 5\r\n",
            b"log rtcm1033b ontime 10\r\n",
            b"log rtcm1075b ontime 1\r\n",
            b"log rtcm1085b ontime 1\r\n",
            b"log rtcm1095b ontime 1\r\n",
            b"log rtcm1115b ontime 1\r\n",
            b"log rtcm1125b ontime 1\r\n",
        ]


    def get_settings(self):
        settings_list = self.settings
        if self.coords_status == "auto":
            settings_list.extend([b"fix auto\r\n"])
        elif self.coords_status == "fix":
            settings_list.extend(["fix position {:.9f} {:.9f} {:.9f}\r\n".format(self.coords[0],self.coords[1],self.coords[2]).encode()])
        print(settings_list)
        return settings_list

    def set_coord(self, lat, long, hight):
        self.coords[0] = lat
        self.coords[1] = long
        self.coords[2] = hight
        print(self.coords)

    def set_coord_status(self, status):
        if status == "fix":
            self.coords_status = "fix"
        else:
            self.coords_status = "auto"