import time

import Controller
import SerialPort
import Parser
import WebServer
import Controller
import SettingsJson
import Settings
import NtripServer
import LocalNtrip
import TcpClient
import FileRecorder
import psutil
import os

port_webserver = 80

def get_process_by_port(port):
    for conn in psutil.net_connections(kind='inet'):
        if conn.laddr.port == port:
            process = psutil.Process(conn.pid)
            return process



if __name__ == "__main__":


    process = get_process_by_port(port_webserver)

    if process:
        print(f"Процесс '{process.name()}' с PID {process.pid} использует порт {port_webserver}")
        os.system(f"fuser -k {process.pid}/tcp")
    else:
        print(f"Порт {port_webserver} не используется ни одним процессом.")

    controller = Controller.Controller()

    serial_port = SerialPort.SerialManager()
    parser = Parser.ParserData()
    webs = WebServer.WebServer(port_webserver)
    settings_json = SettingsJson.SettingJson()
    settings = Settings.SettingsManager()
    ntrip_remote = NtripServer.NtripManager()
    ntrip_local = LocalNtrip.LocalNtrip()
    tcp_cli = TcpClient.TcpClient()
    frecord = FileRecorder.FileRecorder()

    parser.set_callback_set_serial(controller.set_serial)

    controller.set_class_reference(serial_port, parser, settings_json, settings, ntrip_remote, ntrip_local, tcp_cli, frecord)
    
    for x in range(100):
        try:
            webs.start()
            break
        except Exception as e:
            print(f"Error: {e} Wait...")
            time.sleep(1)
        
    try:
        while True:
            time.sleep(10)
    except Exception:
        controller.stop_controller()
