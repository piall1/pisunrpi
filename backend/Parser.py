import queue
import threading

class ParserData:
    data_queue = queue.Queue()
    is_running = False
    coords_current = [0.0, 0.0, 0.0]
    pos_stat = "undef"
    sats = "0"
    
    def __init__(self):
        self.func = None
        self.thread = None

    def Start(self):
        self.is_running = True
        self.thread = threading.Thread(target=self.thread_data)
        self.thread.start()

    def stop(self):
        self.is_running = False
        if self.thread is not None:
            self.thread.join()

    def set_serial(self, serial):
        self.func(serial)

    def set_callback_set_serial(self, func):
        self.func = func

    def thread_data(self):
        while self.is_running:
            try:
                data_out = self.data_queue.get(timeout=1)
            except Exception:
                continue
            
            if b"GPSCARD" in data_out:
                data = data_out[data_out.index(b"GPSCARD"):]
                fields = data.decode('latin-1', errors='ignore').split()
                if len(fields) >= 3:
                    field_3 = fields[2].strip("\"")
                    field_3 = field_3[:8]
                    serial_forname = field_3
                    if not self.is_running:
                        continue
                    # self.serial_num = serial_forname
                    self.set_serial(serial_forname)
            if b"#BESTPOSA" in data_out:
                data = data_out[data_out.index(b"#BESTPOSA"):]
                fields = data.decode('latin-1', errors='ignore').split(',')
                if len(fields) >= 23:
                    self.pos_stat = fields[10]
                    self.coords_current[0] = fields[11]
                    self.coords_current[1] = fields[12]
                    self.coords_current[2] = fields[13]
                    self.sats = fields[22]
                    
    def append_data(self, bytearray_data):
        self.data_queue.put(bytearray_data)