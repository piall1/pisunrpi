import os
import subprocess
import socket
import time



class LocalNtrip():

    tcpserverPort = 5201

    tcp_out_status = False
    tcp_out_port = 12345

    ntripPort = 2101
    ntripMountpoint = 'PiGO'
    ntripStr = '\"RTCM3;RTCM 3;;2;GPS+GLONASS+GALILEO+BDS;IQProxy;RUS;53.4632;51.4750;1;0;IQProxy;;B;Y;;\"'
    ntripUser = 1234
    ntripPassword = 1234

    process = None

    def change_mountpoint(self, new_moutpoint):
        self.ntripMountpoint = new_moutpoint

    def check_ports(self):
        counter = 0
        while counter < 10000:
            try:
                with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as s:
                    s.bind(("127.0.0.1", self.tcpserverPort))
                    s.listen(1)
                    print(f"Порт {self.tcpserverPort} доступен.")
                    return True
            except OSError as e:
                print(f"Порт {self.tcpserverPort} занят. Пробуем следующий.")
                self.tcpserverPort += 1
                counter += 1
        return False   

    def set_tcpout(self, tcp_out_status: bool, tcp_out_port: int):
        self.tcp_out_status = tcp_out_status
        self.tcp_out_port = tcp_out_port

    def launch_str2str(self):
        self.check_ports()
        gen_in = f'tcpsvr://:{str(self.tcpserverPort)}' ## параметры входа для программы str2str
        str2str_executable = "./str2str"  # имя исполняемого файла или полный путь к нему
        gen_out = f'ntripc://{str(self.ntripUser)}:{str(self.ntripPassword)}@:{str(self.ntripPort)}/{str(self.ntripMountpoint)}:{str(self.ntripStr)}'
        if self.tcp_out_status:
            gen_out_tcp = f'tcpsvr://:{self.tcp_out_port}'
            gen_launch = [str2str_executable, "-in", gen_in, "-out", gen_out, "-out", gen_out_tcp]
        else:
            gen_launch = [str2str_executable, "-in", gen_in, "-out", gen_out]
        print("Запускаю str2str: ", " ".join(gen_launch))
        self.process = subprocess.Popen(gen_launch)

    def stop_str2str(self):
        if self.process is not None:
            returncode = self.process.poll()
            if returncode is None:
                 self.process.terminate()
                 try:
                    self.process.wait(timeout=5)
                    print("Процесс успешно остановлен.")
                    return True
                 except subprocess.TimeoutExpired:
                    print("Не удалось остановить процесс. Превышено время ожидания.")
                    return False
            else:
                print("Процесс в данный момент не активен.")
                return False
        else:
            print("Процесс еще не был запущен.")
            return False


