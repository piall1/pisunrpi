import queue
import threading
import socket


class TcpClient():
    server_port = 5201
    server_host = "localhost"
    data_queue = queue.Queue()
    thread = None
    is_running = False
    
    def start_tcp(self):
        self.is_running = True
        self.thread = threading.Thread(target=self.thread_tcpserver)
        self.thread.start()
        print("TCP START")

    def stop_tcp(self):
        if not self.is_running:
            print("tcp не запущен")
            return
        self.is_running = False
        if self.thread is not None:
            self.thread.join()
        self.disconnect()

    def set_tcp_port(self, tcp_port):
        self.server_port = tcp_port

    

    def connect_tcp(self):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect((self.server_host,self.server_port))
        # data = self.client_socket.recv(100)
        # print("Сервер ответил:", data)

    def disconnect(self):
        self.client_socket.close()

    def thread_tcpserver(self):
        while self.is_running:
            data_buffer = b''
            try:
                data = self.data_queue.get(timeout=1)
            except queue.Empty:
                print("queue.Empty ")
                data = b''
            data_buffer += data
            ret = self.client_socket.send(data_buffer)
            err_code = self.client_socket.getsockopt(socket.SOL_SOCKET, socket.SO_ERROR)
            # print(ret, err_code)
            # print(data_buffer)
            data_buffer = b''

    def append_data(self, bytearray_data):
        if self.is_running:
            self.data_queue.put(bytearray_data)

