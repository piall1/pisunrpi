import os
import threading
import time
import SerialPort
import Parser
import SettingsJson
import Settings
import NtripServer
import TcpClient
import LocalNtrip
import FileRecorder

class Controller:
    _instance = None
    connect_status = "ready"
    status_primary_potok = "close"
    ntrip_work_state = "disable"        # disable remote local
    serial = ""
    portname = ""
    ntrip_status = ""
    tcp = ""
    tcp_port = ""
    coord_status = ""
    coords = ""
    func = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Controller, cls).__new__(cls)
            
        return cls._instance

    def set_class_reference(self, serial_p: SerialPort.SerialManager, 
                                  parser: Parser.ParserData,
                                  settings_json: SettingsJson.SettingJson,
                                  settings: Settings.SettingsManager,
                                  ntrip_remote: NtripServer.NtripManager,
                                  ntrip_local: LocalNtrip.LocalNtrip,
                                  tcp_client: TcpClient.TcpClient,
                                  file_recorder: FileRecorder.FileRecorder):
        self.ser = serial_p
        self.parser = parser
        self.settings_json = settings_json
        self.settings = settings
        self.ntrip_remote = ntrip_remote
        self.ntrip_local = ntrip_local
        self.tcp_client = tcp_client
        self.file_recorder = file_recorder

    def set_serial(self, serial):
        self.serial = serial

    def get_status_work(self):
        if self.connect_status == "ready":
            return "default"
        elif self.connect_status == "connected":
            return "processing"

    def get_ports(self):
        return self.ser.get_ports()
    
    def get_serial_n(self):
        return str(self.serial)

    def get_sats(self):
        return self.parser.sats
    
    def get_fix_status(self):
        return self.parser.pos_stat
    
    def get_console_data(self):
        return [""]     
       
    def get_current_coords(self):
        return self.parser.coords_current
    
    def get_ntrip_status(self):
        if self.ntrip_work_state == "local":
            tcpstat = "on" if self.ntrip_local.tcp_out_status == True else "off"
            str = f"Local: localhost:2101 Mountpoint: {self.ntrip_local.ntripMountpoint} TCP: {tcpstat} port {self.ntrip_local.tcp_out_port} "
        elif self.ntrip_work_state == "remote":
            str = f"Remote {self.ntrip_remote.host}:{self.ntrip_remote.port} mountpoint {self.ntrip_remote.mountpoint}"
        else:
            str = self.ntrip_work_state
        return str

    def format_size(self, size_bytes):
        suffixes = ['B', 'KB', 'MB', 'GB', 'TB']
        i = 0
        while size_bytes >= 1024 and i < len(suffixes)-1:
            size_bytes /= 1024.0
            i += 1
        return f"{size_bytes:.2f} {suffixes[i]}"

    def get_file_status(self):
        if self.file_recorder.status_file == "open":
            file_path = os.path.join(self.file_recorder.file_path, self.file_recorder.filename)
            file_size = "error"
            if os.path.exists(file_path):
                file_size = os.path.getsize(file_path)
            if type(file_size) == int:
                file_size = self.format_size(file_size)
            str = f"Current file: {self.file_recorder.filename} Size: {file_size}"
        if self.file_recorder.status_file == "close":
            str = f"stoped"
        return str

    def save_params(self, coords, coords_status):
        print(coords, coords_status)
        self.settings_json.set_coords(coords[0], coords[1], coords[2])
        self.settings_json.set_coords_status(coords_status)
        self.settings_json.write_settings()
    
    def load_params(self):
        coords_status, coords = self.settings_json.get_params()
        return coords_status, coords

    def read_coords(self):
        self.settings_json.read_settings()

    def configure_work(self, name, value):
        if name == 'portname':
            self.portname = value
        if name == 'ntrip_status':
            self.ntrip_status = value
        if name == 'tcp':
            self.tcp = value
        if name == 'tcp_port':
            self.tcp_port = value
        if name == 'coord_status':
            self.coord_status = value
        if name == 'coords':
            self.coords = value


    def start_data_hub(self):
        self.thread_ntrip = threading.Thread(target=self.data_hub_thread)
        self.exit_close = "open"
        self.thread_ntrip.start()

    def data_hub_thread(self):
        while self.exit_close == "open":
            try:
                deta_recive = b''
                data = self.ser.upload()
                deta_recive += data
                self.ntrip_remote.append_data(deta_recive)
                self.parser.append_data(deta_recive)
                self.file_recorder.write(deta_recive)
                self.tcp_client.append_data(deta_recive)
            except Exception as e:
                print(f"Fail: {e}")

    def stop_primary_potok(self):
        self.exit_close = "close"


    def start_controller(self, port_name, ntrip_status, tcp, tcp_port, coords_status, coords_input):
        print(port_name, ntrip_status, tcp, tcp_port, coords_status, coords_input)
        if self.connect_status == "connected":
            print("Уже нажата...")
            return

        if len(port_name) == 0:
            print("Нулоевая длина имени порта")
            return
            
        ret = self.ser.connect(port_name)
        if ret == False:
            print("Ошибка соединения с COM портом")

        self.connect_status = "connected"


        self.settings.set_coord(coords_input[0], coords_input[1], coords_input[2])
        self.settings.set_coord_status(str(coords_status))
        setting_list = self.settings.get_settings()
        self.ser.set_settings(setting_list)

        self.ser.start_read()
        print("Port Start")

        self.parser.Start()
        print("Start Parser")

        self.start_data_hub()
        print("start primery potok")

        while self.serial == "":
            time.sleep(1)

        if ntrip_status == "remote":
            self.ntrip_work_state = "remote"
            self.ntrip_remote.set_settings("pidt.net", 2101, "1234", "PH" + self.serial)
            self.ntrip_remote.make_ntrip_request()
            self.ntrip_remote.connect()
            self.ntrip_remote.start()
            print("ntrip remote started")

        elif ntrip_status == "local":
            self.ntrip_work_state = "local"
            self.ntrip_local.change_mountpoint("PH")
            if tcp == "on":
                tcp_bool = True
            else:    
                tcp_bool = False
            self.ntrip_local.set_tcpout(tcp_bool, tcp_port)
            self.ntrip_local.launch_str2str()
            self.tcp_client.set_tcp_port(self.ntrip_local.tcpserverPort)
            time.sleep(0.1)
            self.tcp_client.connect_tcp()
            self.tcp_client.start_tcp()
            print("ntrip local started")

        self.file_recorder.make_name()
        self.file_recorder.open_file()
        print("Начало работы")
    
    def stop_controller(self):
        if self.connect_status == "ready":
            print("Не работает...")
            return
        self.ntrip_work_state = "disable"
        self.connect_status = "ready"
        self.stop_primary_potok()
        self.file_recorder.close_file()
        self.ser.stop_read()
        self.parser.stop()
        self.tcp_client.stop_tcp()
        self.ntrip_remote.stop()
        self.ntrip_local.stop_str2str()
        print("Конец работы")

    def delete_static_file(self, filename):
        try:
            file_path = os.path.join(self.file_recorder.file_path, filename)
            if os.path.exists(file_path):
                os.remove(file_path)
                print(f"Файл {filename} успешно удален.")
            else:
                print(f"Файл {filename} не существует.")
        except Exception as e:
            print(f"Ошибка при удалении файла {filename}: {e}")
    

