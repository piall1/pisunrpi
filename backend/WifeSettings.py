import json
import os

class WifeSettings:
    
    settings_path = "./wifi_spisok.json"
    wifi_list = []
    wifi_settings = { 
        'WiFi1': 'Password1',
        'WiFi2': 'Password2',
    }
   

    def __init__(self):
        
        size = 0
        try:
            size = os.path.getsize(self.settings_path)
        except FileNotFoundError:
            print("файла нет, size будет равен 0 потомучто мы его задали до try = 0 ")

        if size == 0:
            print("Файл пустой, записываем дефолтные значения")
            self.write_settings(self.wifi_settings)
        else:
            print("Файл не пустой! Считаваем данные!")
            try: 
                self.read_settings()
            except Exception:
                print("Ошибка чтения! Записываю дефолтный конфиг")
                self.write_settings(self.wifi_settings)

    def write_settings(self, wifi_settings):
        dictionary = {'wifi': wifi_settings}
        jsonString = json.dumps(dictionary,ensure_ascii=False, indent=4)
        self.write(jsonString)

    def read_settings(self):
        json_string = self.read()
        decoded_data = json.loads(json_string)
        wifi_settings = decoded_data['wifi']
    
        for wifi_name, wifi_password in wifi_settings.items():
            self.wifi_list.append((wifi_name, wifi_password))
        return self.wifi_list    
        
    def write(self, str_data):
        with open(self.settings_path, "w") as file:
            file.write(str_data)

    def read(self):
        with open(self.settings_path, "r") as file:
            data = file.read()
            return data

    def get_wifi_setting(self):
        pass

    def add_wifi_setting(self):
        pass

    def remove_wifi_setting(self):
        pass

    def save_wifi_setting(self):
        pass

    def set_wifi_setting(self):
        pass
        