import http.server
import socketserver
import time
import WebJson
import Controller
import os

base_path = "../public/"

class MyRequestHandler(http.server.BaseHTTPRequestHandler):
    
    def header_send(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()
    
    def sendfile(self, file_path, content_type):
        print("file for responce: ", file_path)
        if os.path.exists(file_path):
            # Файл существует, читаем его содержимое и отправляем
            with open(file_path, 'rb') as file:
                self.send_response(200)
                self.send_header('Content-type', content_type)
                self.end_headers()
                self.wfile.write(file.read())
        else:
            # Файл не найден, отправляем ошибку 404
            self.send_error(404, 'File Not Found')
    
    def do_GET(self):
        print("Recive GET request", self.path)
        cont = Controller.Controller()
        web_json = WebJson.WebJson()

        if self.path == '/api/status/get': #отдать статусы разные
            # запрос разных статусов работы
            self.header_send()
            work_status = cont.get_status_work()
            coords = cont.get_current_coords()
            ntrip_status = cont.get_ntrip_status()
            file_status = cont.get_file_status()
            ports = cont.get_ports()
            serial_n = cont.get_serial_n()
            sats = cont.get_sats()
            fix_status = cont.get_fix_status()
            console = cont.get_console_data()
            string = web_json.create_status(work_status, coords, ntrip_status, file_status, ports, serial_n, sats, fix_status, console)
            self.wfile.write(string.encode('utf-8'))
        elif self.path == '/api/files/list':
            self.header_send()
            string = web_json.create_filelist(cont.file_recorder.file_path)
            self.wfile.write(string.encode('utf-8'))
        elif self.path == '/api/params/get':
            # отдать параметры из файла
            self.header_send()
            coords_status, coords = cont.load_params()
            string = web_json.create_params(coords, coords_status)
            self.wfile.write(string.encode('utf-8'))
        elif self.path == '/':
            # Отдаем главную HTML страницу
            file_path = os.path.join(base_path, 'index.html')
            content_type = 'text/html'
            self.sendfile(file_path, content_type)      
        elif self.path.endswith('.html'):
            # Отдаем JavaScript файл
            file_path = os.path.join(base_path, self.path.lstrip('/'))
            content_type = 'text/html'
            self.sendfile(file_path, content_type)
        elif self.path.endswith('.js'):
            # Отдаем JavaScript файл
            file_path = os.path.join(base_path, self.path.lstrip('/'))
            content_type = 'application/javascript'
            self.sendfile(file_path, content_type)
        elif self.path.endswith('.css'):
            # Отдаем CSS файл
            file_path = os.path.join(base_path, self.path.lstrip('/'))
            content_type = 'text/css'
            self.sendfile(file_path, content_type)
        elif self.path.endswith('.ico'):
            # Отдаем файл .iso
            file_path = os.path.join(base_path, self.path.lstrip('/'))
            content_type = 'image/ico'
            self.sendfile(file_path, content_type)
        elif self.path.endswith('.png'):
            # Отдаем файл PNG
            file_path = os.path.join(base_path, self.path.lstrip('/'))
            content_type = 'image/png'
            self.sendfile(file_path, content_type)
        elif self.path.endswith('.map'):
            # Отдаем файл MAP
            file_path = os.path.join(base_path, self.path.lstrip('/'))
            content_type = 'file/map'
            self.sendfile(file_path, content_type)
        elif self.path.endswith('.rtcm3'):
            # Отдаем файл RTCM3
            elements = self.path.split("/")
            last_element = elements[-1]
            file_path = cont.file_recorder.file_path + last_element
            content_type = 'file/rtcm3'
            self.sendfile(file_path, content_type)            
        else:
            self.send_error(404, 'File Not Found')
        
    def do_POST(self):
        content_length = int(self.headers['Content-Length'])
        post_data = self.rfile.read(content_length).decode('utf-8')
        print("Recive POST request", post_data)
        cont = Controller.Controller()
        web_json = WebJson.WebJson()

        if self.path == '/test':
            self.header_send()
            self.wfile.write(f'Post Data: {post_data}'.encode('utf-8'))
        elif self.path == '/api/params/set':
            try:
                self.header_send()
                coords_status, coords = web_json.parce_params(post_data)
                cont.save_params(coords, coords_status)
                var = '''{ "status": "ok", "message": "Параметры сохранены"}'''
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка сохранения параметров", e)
                var =  ''' {"status": "error", "message": "Ошибка сохранения параметров!"} '''
                self.wfile.write(var.encode('utf-8'))
        elif self.path == '/api/pisatel/start':
            try:
                self.header_send()
                port_name, ntrip_status, tcp, tcp_port, coords_status, coords_input = web_json.parce_start_message(post_data)
                print(port_name, ntrip_status, tcp, tcp_port, coords_status, coords_input)
                cont.start_controller(port_name, ntrip_status, tcp, tcp_port, coords_status, coords_input)
                var = '''{ "status": "ok", "message": "Начало работы"}'''
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка начала работы", e)
                var = "{ \"status\": \"error\", \"message\": \"Ошибка начала работы!\"" + f"{e}" + "}"
                self.wfile.write(var.encode('utf-8'))            
        elif self.path == '/api/pisatel/stop':
            try:
                self.header_send()
                cont.stop_controller()
                var = '''{ "status": "ok", "message": "Работа остановлена"}'''
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                var = "{ \"status\": \"error\", \"message\": \"Ошибка остановки работы!\"" + f"{e}" + "}"
                self.wfile.write(var.encode('utf-8'))            
        elif self.path == '/api/files/delete':
            try:
                self.header_send()
                filename = web_json.parce_delete_file(post_data)
                cont.delete_static_file(filename)
                var = "{ \"status\": \"ok\", \"message\": \"Файл " + f"{filename}" + "удален\"}"
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                var = "{ \"status\": \"error\", \"message\": \"Ошибка удаления файла!\"" + f"{e}" + "}"
                self.wfile.write(var.encode('utf-8'))
                
class WebServer:
    def __init__(self, port):
        self.port = port
        self.handler = None
        self.server = None

    def __del__(self):
        self.stop()

    def start(self):
        self.handler = MyRequestHandler
        self.server = socketserver.TCPServer(('', self.port), self.handler)
        print(f"Server started on port {self.port}")
        self.server.serve_forever()
        
    def stop(self):
        if self.server:
            print("Server stopping...")
            self.server.shutdown()
            self.server.server_close()
            print("Server stopped.")