import json
import os

class WifiSettings:

    settings_path = "./wifi_spisok.json"
    wifi_settings = []
    
    def __init__(self):
        self.wifi_settings = self.read_settings() or []

    def write_settings(self, wifi_settings):
        with open(self.settings_path, "w") as file:
            json.dump(wifi_settings, file, ensure_ascii = False, indent=4)

    def read_settings(self):
        try:
            with open(self.settings_path, "r") as file:
                return json.load(file)
        except (FileNotFoundError, json.JSONDecodeError):
            return None

    def add_wifi_setting(self, wifi_name, wifi_ip, wifi_password):
        for setting in self.wifi_settings:
            if setting['ssid'] == wifi_name:
                setting['ip'] = wifi_ip
                setting['password'] = wifi_password
                self.write_settings(self.wifi_settings)
                return

        wifi_setting = {
            "ssid": wifi_name,
            "ip": wifi_ip,
            "password": wifi_password
        }

        self.wifi_settings.append(wifi_setting)
        self.write_settings(self.wifi_settings)

    def get_wifi_settings(self):
        wifi_settings_list = []

        for wifi_setting in self.wifi_settings:
            ssid = wifi_setting.get("ssid", "")
            ip = wifi_setting.get("ip", "")
            password = wifi_setting.get("password", "")
            wifi_settings_list.append((ssid, ip, password))

        return wifi_settings_list

    def remove_wifi_setting(self, wifi_name):
        for setting in self.wifi_settings:
            if setting.get('ssid') == wifi_name:
                self.wifi_settings.remove(setting)
                self.write_settings(self.wifi_settings)
                return

        print(f"Настройка с ssid '{wifi_name}' не найдена в списке")

        