import queue
import threading
import socket
import base64
import time
from concurrent.futures import thread

class NtripManager:
    is_running = False
    data_queue = queue.Queue()
    client_socket = None
    ntrip_request = ""
    host = ""
    port = ""
    password = ""
    mountpoint = ""

    def __init__(self):
        self.thread = None

    def set_settings(self, host, port, pwd, mount):
        self.host = host
        self.port = port
        self.password = pwd
        self.mountpoint = mount
        #print(self.host, self.port, self.password, self.mountpoint)
        #print("Tyt sdelai peredachu nastroek v obiekt klassa")

    def make_ntrip_request(self):
        self.ntrip_request = f'SOURCE {self.password} {self.mountpoint}\r\n'
        self.ntrip_request += 'Source-Agent: NTRIP PiSun\r\n'
        self.ntrip_request += 'STR: RTCM3;RTCM 3;;2;GPS+GLONASS+GALILEO+BDS;PiGO;RUS;53.4632;51.4750;1;0;IQProxy;;B;Y;;\r\n'
        self.ntrip_request += '\r\n\r\n'
        #print("Сформировали ntrip_request:", self.ntrip_request)

    def connect(self):
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect((self.host, self.port))
        self.client_socket.sendall(self.ntrip_request.encode())
        data = self.client_socket.recv(100)
        print("Сервер ответил:", data)

    def reconnect(self):
        self.disconnect()
        self.connect()
        
    def disconnect(self):
        self.client_socket.close()

    def start(self):
        self.is_running = True
        self.thread = threading.Thread(target=self.thread_function)
        self.thread.start()

    def stop(self):
        if not self.is_running:
            print("NtripManager not active! Return.")
            return
        self.is_running = False
        if self.thread is not None:
            self.thread.join()
        self.disconnect()
        self.thread = None
        #print("Закрыт Ntrip сервер")

    def thread_function(self):
        data_buffer = b''
        while self.is_running:
            try:
                data = self.data_queue.get(timeout=1)
                data_buffer += data
                self.client_socket.send(data_buffer)
            except queue.Empty:
                pass
            except Exception as e:
                print(f"Error sending to caster [{e}]. Reconnect...")
                self.reconnect()
            #self.client_socket.getsockopt(socket.SOL_SOCKET, socket.SO_ERROR)
            #print(ret, err_code)
            #print(data_buffer)
            data_buffer = b''

    def append_data(self, bytearray_data):
        self.data_queue.put(bytearray_data)


