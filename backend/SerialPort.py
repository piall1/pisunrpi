import queue
import threading
import time
from threading import Thread
import serial.tools.list_ports


class SerialManager:
    is_connect = False
    is_run = False
    data_queue = queue.Queue()

    def __init__(self):
        self.thread = None


    def get_ports(self):
        ports = [port.device for port in serial.tools.list_ports.comports()]
        ports. append("/dev/ttyS0")
        return ports

    def set_setting(self,setting):
        exit_flag = False
        answer_find = False
        data = bytearray(100)
        self.ser.timeout = 0.1
        self.ser.flush()
        self.ser.write(setting)

        data_buffer = b''
        start_time = time.time()

        while not exit_flag:
            num_bytes = self.ser.readinto(data)
            data_buffer += data

            if b"OK!" in data_buffer:
                exit_flag = True
                answer_find = True
            elif time.time() - start_time > 5:
                exit_flag = True
        return answer_find

    def set_settings(self, settings_list):
        for setting in settings_list:
            result = self.set_setting(setting)
            print("set_settings:", setting, "result:", result)

    def connect(self, port_name):
        if self.is_connect:
            print("Уже есть коннект!!!")
            return self.is_connect
        try:
            self.ser = serial.Serial(port_name, 115200)
            if self.ser.isOpen():
                self.is_connect = True
                print("Успешное соединение!")
            else:
                print("Не удалось открыть порт")
        except serial.serialutil.SerialException as e:
            print("Ошибка при подключении: ", str(e))
        return self.is_connect

    def start_read(self):
        self.is_run = True
        self.thread = threading.Thread(target=self.thread_read)
        self.thread.start()
        #print("Началась запись с компорта")

    def thread_read(self):
        if self.ser:
            while self.is_run:
                data = bytearray(1000)
                self.ser.timeout = 0.1
                num_bytes = self.ser.readinto(data)
                data = data[:num_bytes]
                self.data_queue.put(data)
                # print("Received:", data)

            # print("Считавание данных с компорта")

    def stop_read(self):
        self.is_run = False
        if self.thread is not None:
            self.thread.join()
        self.ser.close()
        self.is_connect = False
        #print("Закончилась запись с компорта")

    def upload(self):
        dataret = b""
        data = self.data_queue.get(timeout=1)
        dataret += data
        lenght = len(dataret)
        # print(f"Upload {lenght} bytes!!!!!!!!!!!!!!")
        return dataret




