import json
import os

class FileParam:
    coords = [0.0, 0.0, 0.0]
    coord_status = "auto"

class SettingJson:

    settings_path = "./settings.json"

    def __init__(self):
        self.params = FileParam()
        size = 0
        try:
            size = os.path.getsize(self.settings_path)
        except FileNotFoundError:
            print("файла нет, size будет равен 0 потомучто мы его задали до try = 0 ")

        if size == 0:
            print("Файл пустой, записываем дефолтные значения")
            self.write_settings()
        else:
            print("Файл не пустой! Считаваем данные!")
            try: 
                self.read_settings()
            except Exception:
                print("Ошибка чтения! Записываю дефолтный конфиг")
                self.write_settings()

    def write_settings(self):
        dictionary = {'coords': {
                                    'lat': self.params.coords[0], 
                                    'lon': self.params.coords[1], 
                                    'height': self.params.coords[2]
                                },
                      'coords_status': self.params.coord_status   
                     }
        jsonString = json.dumps(dictionary, indent=4)
        self.write(jsonString)

    def read_settings(self):
        json_string = self.read()
        decoded_data = json.loads(json_string)
        
        self.params.coord_status = decoded_data['coords_status']
        coords = decoded_data['coords']
        
        self.params.coords[0] = coords['lat']
        self.params.coords[1] = coords['lon']
        self.params.coords[2] = coords['height']

    def get_params(self):
        return self.params.coord_status, tuple(self.params.coords)
    
    # def get_coords(self):
    #     return self.params.coords

    # def get_coords_status(self):
    #     return self.params.coord_status

    def set_coords(self, lat, lon, height):
        self.params.coords[0] = lat
        self.params.coords[1] = lon
        self.params.coords[2] = height

    def set_coords_status(self, coord_status):
        self.params.coord_status = coord_status

    def write(self, str_data):
        with open(self.settings_path, "w") as file:
            file.write(str_data)

    def read(self):
        with open(self.settings_path, "r") as file:
            data = file.read()
            return data

